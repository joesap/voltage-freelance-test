## To run the app:
1. Clone it to your computer.
2. cd into the root directory.
3. Run ` npm install`.
4. Run `gulp smush` (this optimizes the images and places them in the build directory).
5. Run `gulp watch`
6. Open `localhost:8080`