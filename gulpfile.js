'use strict';

var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    pump = require('pump'),
    sass = require('gulp-sass'),
    connect = require('gulp-connect'),
    imagemin = require('gulp-imagemin'),
    watch = require('gulp-watch');

// make vars for file paths
var paths = {
  source: 'src/assets/',
  dest: 'build/' 
};

// minify JS
gulp.task('compress', function(callback) {
  pump([
      gulp.src(paths.source + 'js/*.js'),
      uglify(),
      gulp.dest(paths.dest + 'js')
    ],
    callback
  )
  .pipe(connect.reload());
});

gulp.task('connect', function() {
  return connect.server({
    root: '.',
    livereload: true
  })
});

gulp.task('html', function() {
  gulp.src('**/*.html')
  .pipe(connect.reload())
});

// compress images
gulp.task('smush', function() {
  gulp.src('src/assets/images/*')
  .pipe(imagemin())
  .pipe(gulp.dest('build/images'))
});

// minify css
// .pipe(sass({outputStyle: 'compressed'})) to minify
gulp.task('sass', function() {
  return gulp.src(paths.source + 'scss/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest(paths.dest + '/css'))
});

gulp.task('build', ['sass', 'compress', 'html']);

gulp.task('watch-and-reload', ['build'], function () {
  watch(['src/**', '**/*.html'], function () {
    gulp.start('build');
  }).pipe(connect.reload());
});

gulp.task('watch', ['build', 'watch-and-reload', 'connect']);
