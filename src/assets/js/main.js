var Main = {

  init: function() {
    this.modal();
  },

  modal: function () {
    var $modal = $('#modal');
    $('#modal-trigger').on('click', function () {
      $modal.addClass('md-show');
    });

    $('.md-close').on('click', function () {
      $modal.removeClass('md-show');
    })
  },

};


(function() {
  Main.init();
})();
